section .text
%define EXIT_CODE 60 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, EXIT_CODE
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.loop:
    cmp byte[rdi+rax], 0x0
    jz .break
    inc rax
    jmp .loop
.break:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    push rdi
    call string_length
    mov rdx, rax
    pop rsi
    mov rax, 1
    mov rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, `\n`
    jmp print_char
   

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rsi, rsp
    sub rsp, 32
    mov rax, rdi
    mov rcx, 10
    dec rsi
    mov byte[rsi], 0
    .loop:
        xor rdx, rdx
        dec rsi
        div rcx
        add dl, '0'
        mov byte[rsi], dl
        test rax, rax
        jne .loop
    mov rdi, rsi
    call print_string
    add rsp, 32
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0x0
    jge .positive
    neg rdi
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    .positive:
	jmp print_uint


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
    .loop:
	mov al, byte[rdi]
	mov cl, byte[rsi]
	cmp al, cl
	jne .not_equals
	cmp al, 0	
	je .equals
	inc rdi
	inc rsi
	jmp .loop
    .equals:
	mov rax, 1
	ret
    .not_equals:
        xor rax, rax
	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rax, 0
    mov rdi, 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    test rax, rax
    jz .end_of_stream
    mov al, [rsp]
    .end_of_stream:
	pop rdi
	ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push r12
    push r13
    push r14
    mov r12, rdi
    mov r13, rsi
    mov r14, rcx
    xor r14, r14
    .next_char:
        call read_char
        cmp rax, ' '
        jz .next_char
        cmp rax, `\t`
        jz .next_char
        cmp rax, `\n`	
        jz .next_char
        jmp .no_chars


    .loop:
        call read_char
        cmp rax, 0x20
        jz .success
        cmp rax, 0x9
        jz .success
        cmp rax, 0xA
        jz .success
        .no_chars:
            test rax, rax
            jz .success
            mov byte[r12+r14], al
            inc r14
            cmp r14, r13
            jna .loop
    xor rax, rax
    jmp .out
 

    .success:
        test r14, r14
        jz .break
        mov byte[r12+r14], 0
        .break:
            mov rax, r12
            mov rdx, r14
	    .out:
                pop r14
                pop r13
                pop r12
                ret    
	
; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    mov r8, 10

    .loop:
        movzx rcx, byte [rdi + rdx]
        test rcx, rcx
        jz .break
        cmp rcx, '0' 
        jl .break
        cmp rcx, '9'  
        jg .break
        push rdx
        mul r8
        pop rdx
        sub rcx, '0'
        add al, cl
        inc rdx
        jmp .loop
    .break:
    	ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte [rdi], '+'
    je .positive_with_plus
    cmp byte [rdi], '-'
    jne .positive
    inc rdi
    call parse_uint
    test rdx, rdx
    jz .zero_length
    neg rax 
    inc rdx
    ret
    .positive:
	call parse_uint
	ret
    .positive_with_plus:
	inc rdi
	call parse_uint
	test rdx, rdx
	jne .zero_length
	inc rdx
	ret
    .zero_length:
	ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi
    cmp rax, rdx
    jnb .too_long
    push rax
    inc rax
    .loop:
	mov cl, [rdi]
	mov [rsi], cl
	inc rsi
	inc rdi
	dec rax
	test rax, rax
	jne .loop
    pop rax
    ret
    .too_long:
	xor rax, rax
	ret
	
